from datetime import datetime, timedelta
import logging
from math import ceil
import os
import sys
import tarfile

import boto3
from botocore.exceptions import ClientError


log = logging.getLogger("backups")
log.propagate = False
file_hdlr = logging.StreamHandler()
formatter = logging.Formatter(
    '(%(asctime)s) - %(levelname)s - %(message)s')
file_hdlr.setFormatter(formatter)
log.addHandler(file_hdlr)
log.setLevel(logging.INFO)

BACKUP_DIRS = os.environ.get('BACKUPS_FOLDERS').split(',')
BUCKET_NAME = os.environ.get('BACKUPS_S3_CONTAINER_NAME')
DELETE_BACKUPS_AFTER = int(os.environ.get('BACKUPS_DELETE_AFTER'))

ACCESS_KEY_ID = os.environ.get('BACKUPS_S3_ACCESS_KEY_ID')
ACCESS_KEY_SECRET = os.environ.get('BACKUPS_S3_ACCESS_KEY_SECRET')
ENDPOINT_URL = os.environ.get('BACKUPS_S3_ENDPOINT_URL')
REGION_NAME = os.environ.get('BACKUPS_S3_REGION_NAME')

OBJECT_SEGMENT_SIZE = 1073741824  # 1GB
MAX_FILE_SIZE = (5 * 1024 * 1024 * 1024 + 2) / 2  # 2.6GB


now = datetime.utcnow()
delete_after = now - timedelta(days=DELETE_BACKUPS_AFTER)

try:
    session = boto3.session.Session()
    client = boto3.client(
        's3',
        aws_access_key_id=ACCESS_KEY_ID,
        aws_secret_access_key=ACCESS_KEY_SECRET,
        region_name=REGION_NAME,
        use_ssl=True,
        endpoint_url=ENDPOINT_URL,
    )
except Exception:
    log.exception("Error during auth and sdk setup.")
    sys.exit(1)

try:
    client.head_bucket(Bucket=BUCKET_NAME)
    objects = client.list_objects_v2(Bucket=BUCKET_NAME).get("Contents", [])

    log.info("Bucket %s exists." % BUCKET_NAME)

    for obj in objects:
        if obj['LastModified'] < delete_after:
            client.delete_object(Bucket=BUCKET_NAME, Key=obj['Key'])
            log.info("Object %s deleted." % object['name'])

except ClientError as err:
    if err.response['ResponseMetadata']['HTTPStatusCode'] == 301:
        raise Exception(
            "Bucket %s exists, but in a different "
            "region than we are connecting to." % REGION_NAME)
    elif err.response['ResponseMetadata']['HTTPStatusCode'] == 404:
        bucket_params = {
          "Bucket": BUCKET_NAME,
        }
        if REGION_NAME != 'us-east-1':
            bucket_params['CreateBucketConfiguration'] = {
                'LocationConstraint': REGION_NAME}
        client.create_bucket(**bucket_params)
    else:
        raise


log.info("Making tarfile.")
tar_file_name = "backup_%s.tar.gz" % datetime.strftime(now, "%Y_%m_%d")
tar_file_location = "/tmp/%s" % tar_file_name
with tarfile.open(tar_file_location, "w:gz") as tar:
    for directory in BACKUP_DIRS:
        tar.add(directory)

file_size = os.path.getsize(tar_file_location)

if file_size > MAX_FILE_SIZE:
    log.info("Uploading backup as multi-part-upload.")
    try:
        mpu_id = client.create_multipart_upload(
            Bucket=BUCKET_NAME, Key=tar_file_name)["UploadId"]

        approx_parts = ceil(file_size / OBJECT_SEGMENT_SIZE)

        parts = []
        uploaded_bytes = 0
        with open(tar_file_location, "rb") as f:
            i = 1
            while True:
                data = f.read(OBJECT_SEGMENT_SIZE)
                if not len(data):
                    break
                part = client.upload_part(
                    Body=data, Bucket=BUCKET_NAME, Key=tar_file_name,
                    UploadId=mpu_id, PartNumber=i)
                parts.append({"PartNumber": i, "ETag": part["ETag"]})
                uploaded_bytes += len(data)
                log.info("%s of %s uploaded" % (i, approx_parts))
                i += 1
        client.complete_multipart_upload(
            Bucket=BUCKET_NAME, Key=tar_file_name, UploadId=mpu_id,
            MultipartUpload={"Parts": parts})
    except Exception:
        log.exception("Error during upload of cattail backups.")
        os.remove(tar_file_location)
        uploads = client.list_multipart_uploads(
            Bucket=BUCKET_NAME).get('Uploads', [])
        log.info("Aborting", len(uploads), "uploads")
        for u in uploads:
            client.abort_multipart_upload(
                Bucket=BUCKET_NAME, Key=u["Key"], UploadId=u["UploadId"])
        sys.exit(1)
else:
    log.info("Uploading backup.")
    try:
        with open(tar_file_location, 'rb') as content:
            client.put_object(
                Key=tar_file_name, Bucket=BUCKET_NAME,
                Body=content)
        os.remove(tar_file_location)
        log.info("Uploaded %s" % tar_file_name)
    except Exception:
        log.exception("Error during upload of cattail backups.")
        os.remove(tar_file_location)
        sys.exit(1)
